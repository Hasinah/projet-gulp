$('.slider-section').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: false,
    arrows:false,
    autoplay:false,
    cssEase: 'linear'
  });