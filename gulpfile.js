var gulp = require('gulp');//on va utiliser la library gulp par la suite ou voir dans le cour avec var $ = requier ('')
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps'); //  lorsque je inscpect l'element me montre dans quel fichier se trouve
var autoprefixer = require('gulp-autoprefixer');// permet de mettre automatiquement le webkit
var browserSync = require('browser-sync').create();// pour un refraiche automatiquement

//sass utlise le compiler nodejs
sass.compiler = require('node-sass');
//creationmatache pour compiler mes fichier en sass en css
//les etoiles pour aller chercher dans le dossier 
// il regarde si il ya des eurreur et nous sort les errurs dans le console
gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
    .pipe(sourcemaps.init())//insitialiser avant le build
    .pipe(sass({
        errLogToConsole: true,
        outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({
        overrideBrowserslist: ['> 0%', 'IE 11'],
        cascade: false
    }))  
    .pipe(sourcemaps.write())//ecrit le source map
    .pipe(gulp.dest('./build/css'))//build
    .pipe(browserSync.stream());
});

gulp.task('html', function () {
    return gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
});
gulp.task('assets', function () {
    return gulp.src('./src/assets/image/*.*')
        .pipe(gulp.dest('./build/image'));
});

gulp.task('js', function () {
    return gulp.src('./src/js/**/*.*')
        .pipe(gulp.dest('./build/js'));
});

//on cree un watcher pour surveiller les changement sur les fichiers
gulp.task('watch', function () {
    browserSync.init({
        server: {
            baseDir: './build'
        }
    })
    //on cree un watcher pour surveiller les changement sur les fichiers
    gulp.watch('./src/**/*.html', gulp.series('html'));
    gulp.watch('./src/sass/**/*.scss', gulp.series('sass')).on('change', browserSync.reload);
});

//one task to rule them all  important l'ordre
gulp.task('default', gulp.parallel('js', 'sass', 'html' ,'assets', 'watch'));// gulp qui fait gulp sass gulp html gulp watch
